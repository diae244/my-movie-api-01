from fastapi import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService

movie_router = APIRouter()


        
@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def getMovies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie, status_code=200)
def getMovieById(id: int = Path(ge = 1, le = 2000)) -> Movie: # Validación Para Parametro Path
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie], status_code=200)
def getMoviesByCategory(category: str = Query(min_length=1, max_length=15)) -> List[Movie]: # Validación Parametro Query
    db = Session()
    result = MovieService(db).get_movie(category)
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

@movie_router.post('/movies/', tags=['movies'], response_model=dict, status_code=200)
def createMovie(movie: Movie) -> dict:
    db = Session() # variable para iniciar sesión
    total_movies = db.query(MovieModel).count()
    movie.id = total_movies + 1
    new_movie = MovieModel(**movie.model_dump()) # Utilizamos modelo y mandamos info a registrar
    
    db.add(new_movie) # Hace consulta al db
    db.commit() # Guarda los cambios hechos 
    return JSONResponse(content={"message": "Movie Succesfully Created"}, status_code=200)
    
@movie_router.delete('/movies/{id}', tags=['movies'], status_code=200)
def deleteMovie(id: int):
    db = Session()
    result =MovieService(db).deleteMovie(id)
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    db.delete(result)
    db.commit()

@movie_router.put('/movies/{id}', tags=['movies'], status_code=200)
def updateMovie(id: int, movie: Movie):
    db = Session()
    result= MovieService(db).update_movie(id)
    # result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    
    # # keys = list(dict(movie).keys())
    # # for item in keys:
    # #     result. = movie.item
    # result.title = movie.title
    # result.overview = movie.overview
    # result.year = movie.year
    # result.rating = movie.rating
    # result.category = movie.category
    # db.commit()
    return JSONResponse(content={"message": "Movie Succesfully Updated"}, status_code=200)