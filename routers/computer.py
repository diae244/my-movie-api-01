from fastapi import APIRouter
from fastapi import Path, Query, Body, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.computer import Computer as ComputerModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer

computer_router = APIRouter()

class Computer(BaseModel):
    id: Optional[int] = None
    brand: str = Field(min_length=1, max_length=30)
    model: str = Field(min_length=1, max_length=30)
    color: str = Field(min_length=1, max_length=30)
    ram: str = Field(min_length=1, max_length=30)
    storage: str = Field(min_length=1, max_length=30)
    
    class Config:
        json_schema_extra = {
            "example": {
                "id": 1,
                "brand": "Apple",
                "model": "Pro M3",
                "color": "Black",
                "ram": "16 GB",
                "storage": "256 GB",
            }
        }

# Get Computers
@computer_router.get('/computers', tags=['computers'], response_model=List[Computer], status_code=200, dependencies=[Depends(JWTBearer())])
def getComputers() -> JSONResponse:
    db = Session()
    result = db.query(ComputerModel).all()
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

# Get Compyters By ID
@computer_router.get('/computers/{id}', tags=['computers'], response_model=Computer, status_code=200, dependencies=[Depends(JWTBearer())])
def getComputersByID(id: int = Path(ge=1, le=2000)) -> JSONResponse:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

# Get Computers By Brand
@computer_router.get('/computers/', tags=['computers'], response_model=List[Computer], status_code=200, dependencies=[Depends(JWTBearer())])
def getComputersByBrand(brand: str = Query(min_length=1, max_length=30)) -> JSONResponse:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.brand == brand).all()
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    return JSONResponse(content=jsonable_encoder(result), status_code=200)

# Create Computer
@computer_router.post('/computers', tags=['computers'], response_model=Computer, status_code=200, dependencies=[Depends(JWTBearer())])
def createComputer(computer: Computer) -> JSONResponse:
    db = Session()
    total_computers = db.query(ComputerModel).count()
    computer.id = total_computers + 1
    new_computer = ComputerModel(**computer.model_dump())
    
    db.add(new_computer)
    db.commit()
    return JSONResponse(content={"message": "Computer Succesfully Created"}, status_code=200)

# Delete Computer 
@computer_router.delete('/computers/{id}', tags=['computers'], response_model=Computer, status_code=200, dependencies=[Depends(JWTBearer())])
def deleteComputer(id: int = Path(ge=1, le=2000)) -> JSONResponse:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    
    db.delete(result)
    db.commit()
    return JSONResponse(content={"message": "Computer Succesfully Deleted"}, status_code=200)
    
# Update Computer    
@computer_router.put('/computers/{id}', tags=['computers'], response_model=Computer, status_code=200, dependencies=[Depends(JWTBearer())])
def updateComputer(id: int = Path(ge=1, le=2000), computer: Computer = Body()) -> JSONResponse:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(content={"message": "Not Found"}, status_code=404)
    
    result.brand = computer.brand
    result.model = computer.model
    result.color = computer.color
    result.ram = computer.ram
    result.storage = computer.storage
    db.commit()
    return JSONResponse(content={"message": "Movie Succesfully Updated"}, status_code=200)