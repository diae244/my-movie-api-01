from fastapi import Request, HTTPException
from fastapi.security import HTTPBearer
from FastAPI.utils.jwt_manager import validate_token

class JWTBearer(HTTPBearer):
    async def _call_(self, request: Request):
        auth = await super()._call_(request)
        data = validate_token(auth.credentials)
        if data['email'] != "admin@gmail.com":
            raise HTTPException(status_code=403, detail="Credenciales Incorrectas")